<?php get_header(); ?>
    <div id="HomepageLoadControl">
        <div class="cb h10"></div>
        <div class="PageContentDetailLeft">

            <div id="SubProductMenu">
                <?php get_sidebar('primary');?>
            </div>
        </div><div class="PageContentDetailRight">

            <div id="SubProductCateHome">
                <div class="Nhomsanpham">
                    <div class="baosanphamindex">
                        <div class="cate">
                            <a href="javascript:void(0)"><?php single_cat_title()?></a>

                            <div class="vach"></div>
                            <!--a class='xemtiep' href=''></a-->
                        </div>
                        <div class="cb"></div>
                        <div class="listNews">
                            <div class="tronglist">

                                <?php
                                $i = 0;
                                if (have_posts()):while (have_posts()):the_post();
                                    ?>

                                    <div class="baosanpham thutu<?php if($i % 3 == 0) $i = 0; echo $i;?>">
                                        <div class="khungAnh">
                                            <a class="khungAnhCrop" href="<?php the_permalink()?>">
                                                <img alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url(); ?>" style="position: absolute; z-index: 1; top: 0px; left: -12px; height: 120px; width: 186px;"></a>
                                        </div>
                                        <a class="title" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php the_title()?></a>
                                        <div class="giasp">Giá tham khảo: <b>Liên hệ</b></div>

                                        <div class="cb"><!----></div>
                                    </div>
                                    <?php $i++;?>
                                <?php endwhile;endif;wp_reset_query();?>
                            </div>

                            <div class="cb"><!----></div></div><div class="cb"><!----></div></div><div class="cb"><!----></div>
                </div>
            </div>

        </div>
        <div class="cb h15"></div>
    </div>
	
	<?php get_footer(); ?>