﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
			<div id="maincontent">
	<!-- Begin Left Column -->
	<div id="leftcolumn">
        <div id="page-menu-left" class="box">
	 		<div class="col25">
		           ﻿<div id="sidebar">
									<?php genesis_after_content();?>

					</div>

            </div>
         </div>
	</div>

	<!-- End Left Column -->
	
	<!-- Begin Content Column -->
	<div id="content">
		<div class="newsleft">
					<ul class="listcat">
							<?php	
							if(have_posts()):while(have_posts()):the_post();
						?>
							<li>						
									<div class="Image">
										<?php if(function_exists("has_post_thumbnail") & has_post_thumbnail()){	?> 
										<?php the_post_thumbnail(array(130,122));?>
									<?php }?>
									</div>
									<div class="news_name"><a href="<?php the_permalink()?>" title="<?php the_title()?>"><?php the_title()?></a></div>
									<div class="introtext"><?php the_content_limit(300,'Chi tiết');?></div>
							</li>	
						<?php endwhile;endif;wp_reset_query();?> 							
					</ul>
					<div class="pagination">
							<?php if(function_exists("wp_pagenavi"))
								wp_pagenavi();							
							?>
						</div>
		</div>
				
	</div>
<!-- begin Content Column section-->
	
	<!-- end Content Column section-->
	<!-- End Content Column -->
		<div id="rightcolumn">
			<?php genesis_after_content_sidebar_wrap(); ?>			
		</div>
	</div>
<?php get_footer(); ?>