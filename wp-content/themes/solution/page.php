﻿<?php get_header(); ?>
    <div id="HomepageLoadControl">
        <div class="cb h10"></div>
        <div class="PageContentDetailLeft">

            <div id="SubProductMenu">
                <?php get_sidebar('primary');?>
            </div>
        </div>
        <div id="trangchitiet">
        <?php
        if (have_posts()):while (have_posts()):the_post();?>
            <div><h1 class="title"><?php the_title()?></h1></div>
            <div class="contentview TextSize">
                <?php
                    the_content();

                ?>
            </div>
            <?php endwhile;endif;wp_reset_query();?>
            <div class="cb"><!----></div>

            <div style="padding-top: 15px;" id="CommonShareDetail">
                <div class="fl pr10">
                    <a class="prev" href="javascript:history.go(-1)">Về trang trước</a>&nbsp;&nbsp;&nbsp;
                    <a href="javascript:ScrollTo()" class="top">Lên đầu trang</a>
                </div>

                <div class="fr">
                    <a href="javascript:void(0)" class="email addthis_button_email at300b" target="_blank" title="Email"><span class="at4-icon aticon-email" style="background-color: rgb(115, 138, 141);"><span>Share on email</span></span>Gửi email</a>
                    <a href="javascript:window.print()" class="print">In trang</a>
                </div>
                <div class="cb"><!----></div>
            </div>
            <div class="cb"><!----></div>

            <div id="SubPageContentOtherItem">
                <div id="DisplayLoadControl1_ctl00_ctl00_SubPageContentOtherItem_PNTrangNoiDung">

                    <div class="baikhac">Các bài viết khác khác:</div>

                </div>
            </div>
        </div>
        <div class="cb h15"></div>
    </div>

<?php get_footer(); ?>