<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 */
genesis_doctype();

genesis_meta();
?>
<meta content="INDEX,FOLLOW" name="robots" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
wp_head(); // we need this for plugins

genesis_title();

?>


<script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.tinyscrollbar.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jcarousellite_1.js"></script>

<script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.galleriffic.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.opacityrollover.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/jush.js" type="text/javascript"></script>



<!--Chinh-->
<link href="<?php bloginfo('stylesheet_directory') ?>/css/_cs.css" rel="stylesheet" />
<link href="<?php bloginfo('stylesheet_directory') ?>/css/css.css" rel="stylesheet" />
<link href="<?php bloginfo('stylesheet_directory') ?>/css/css.css" rel="stylesheet" />
<link href="<?php bloginfo('stylesheet_directory') ?>/css/tinyscrollbar.css" rel="stylesheet" />
<link href="<?php bloginfo('stylesheet_directory') ?>/css/chong.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('stylesheet_directory') ?>/css/chong2.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('stylesheet_directory') ?>/css/vo.css" rel="stylesheet" type="text/css"/>
<link href="<?php bloginfo('stylesheet_directory') ?>/css/FontFace.css" rel="stylesheet" type="text/css" />


<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5aa53fca2436c400135c0e36&product=social-ab' async='async'></script>
</head>
<body class="home blog">
    <div class="PageContentDetail">
        <div id="CommonHeader<?php if(!is_home()) echo "2"?>">
            <div class="TopCommonHeader">
                <div class="ContentTopHeader">
                    <div class="leftContentToptHeader">
                        <div id="AdvLogo" class="HideContent">
                            <a href="/"><img alt="LOGO " src="<?php bloginfo('stylesheet_directory') ?>/images/banner1.jpg" class=""></a>
                        </div>
                    </div>
                    <div class="cb"></div>
                </div>
            </div><div class="BottomCommonHeader<?php if(!is_home()) echo "2"?>">
                <div id="CommonMenuMain">
                    <div class="MainContent">
                        <div class="Main ">
                            <div class="Content">
                                <div id="smoothmenumain" class="ddsmoothmenu-v">
                                    <?php
                                    if ( genesis_get_option('nav_type') == 'nav-menu' && function_exists('wp_nav_menu') ) {

                                        $nav = wp_nav_menu(array(
                                            'theme_location' => 'primary',
                                            'container' => '',
                                            'menu_class' => genesis_get_option('nav_superfish') ? 'nav navbar-nav top-nav' : 'nav',
                                            'echo' => 0
                                        ));
                                    } else {
                                        $nav = genesis_nav(array(
                                            'theme_location' => 'primary',
                                            'menu_class' => genesis_get_option('nav_superfish') ? 'nav navbar-nav' : 'nav',
                                            'show_home' => genesis_get_option('nav_home'),
                                            'type' => genesis_get_option('nav_type'),
                                            'sort_column' => genesis_get_option('nav_pages_sort'),
                                            'orderby' => genesis_get_option('nav_categories_sort'),
                                            'depth' => genesis_get_option('nav_depth'),
                                            'exclude' => genesis_get_option('nav_exclude'),
                                            'include' => genesis_get_option('nav_include'),
                                            'echo' => false
                                        ));

                                    }

                                    echo $nav;
                                    ?>
                                </div>
                                <div class="cb"><!----></div>
                            </div>
                        </div>
                    </div>
                </div><div class="cb h15"></div>
                <?php
                    if(is_home()){
                ?>
                <div id="AdvBackgroundSlide">
                    <div class="AdvBackgroundSlideContent">
                        <div class="border_box">
                            <?php if(function_exists("get_smooth_slider_category")){get_smooth_slider_category($catg_slug="slider");}?>
                        </div>
                    </div>
                    <div class="DuoiSlide"></div>
                </div>

                <div class="cb"></div>        <div class="BaoLienHE">
                    <div class="Hotline">HOTLINE: <span> 093.464.8381 - 0903.269.558 </span></div>
                    <div class="EmailHeThong">EMAIL:<span> thepbacmy@gmail.com</span></div>
                </div>
                <?php }else{
                        $the_cat = get_the_category();

                        $category_name = $the_cat[0]->cat_name;

                        $category_link = get_category_link( $the_cat[0]->cat_ID );
                        ?>
                        <div class="cb h3"></div>
                        <div class="BaoLienHE">
                            <div class="Hotline">HOTLINE: <span> 093.464.8381 - 0903.269.558 </span></div>
                            <div class="EmailHeThong">EMAIL:<span> thepbacmy@gmail.com</span></div>

                            <div id="CommonRoad">
                                <div class="road">
                                    <span class="lv1 trangchu"></span><a class="lv1" href="/" title="Trang chủ">Trang chủ</a>
                                    <a class="lv1 arrow" href="<?php echo $category_link;?>" title="<?php echo $category_name;?>"><?php if(is_page()){the_title();}else{single_cat_title();}?></a>                </div>
                            </div>
                        </div>

                <?php }?>

            </div>
        </div>
        <div class="cb"></div>