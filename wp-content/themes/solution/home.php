<?php get_header(); ?>

    <div id="HomepageLoadControl">
        <div class="cb h10"></div>
        <div class="PageContentDetailLeft">


            <div id="SubProductMenu">
                <?php get_sidebar('primary');?>
            </div>

        </div>    <div class="PageContentDetailRight">



            <div id="SubProductCateHome">
                <div class="Nhomsanpham">
                    <div class="baosanphamindex">
                        <div class="cate"><a href="san-pham-noi-bat">Sản phẩm nổi bật</a>
                            <div class="vach"></div>
                            <a class="xemtiep" href="san-pham-noi-bat"></a>
                        </div>
                        <div class="cb"></div>
                        <div class="listNews">
                            <div class="tronglist">

                            <?php
                            $i = 0;
                            query_posts('cat=5&posts_per_page=8&orderby=id');
                            if (have_posts()):while (have_posts()):the_post();
                                ?>

                                <div class="baosanpham thutu<?php if($i % 4 == 0) $i = 0; echo $i;?>">
                                    <div class="khungAnh">
                                        <a class="khungAnhCrop" href="<?php the_permalink()?>">
                                            <img alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url(); ?>" style="position: absolute; z-index: 1; top: 0px; left: -12px; height: 120px; width: 186px;"></a>
                                    </div>
                                    <a class="title" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php the_title()?></a>
                                    <div class="giasp">Giá tham khảo: <b>Liên hệ</b></div>

                                    <div class="cb"><!----></div>
                                </div>
                            <?php $i++;?>
                            <?php endwhile;endif;wp_reset_query();?>
                            </div>
                            <div class="cb"><!----></div>
                        </div>
                        <div class="cb"><!----></div>
                    </div>
                    <div class="cb"><!----></div>

                    <!--khung 2-->

                    <div class="baosanphamindex">
                        <div class="cate"><a href="san-pham">Sản phẩm mới</a>
                            <div class="vach"></div>
                            <a class="xemtiep" href="san-pham"></a>
                        </div>
                        <div class="cb"></div>
                        <div class="listNews">
                            <div class="tronglist">

                                <?php
                                $i = 0;
                                query_posts('cat=3&posts_per_page=8&orderby=id');
                                if (have_posts()):while (have_posts()):the_post();
                                    ?>

                                    <div class="baosanpham thutu<?php if($i % 4 == 0) $i = 0; echo $i;?>">
                                        <div class="khungAnh">
                                            <a class="khungAnhCrop" href="<?php the_permalink()?>">
                                                <img alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url(); ?>" style="position: absolute; z-index: 1; top: 0px; left: -12px; height: 120px; width: 186px;"></a>
                                        </div>
                                        <a class="title" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php the_title()?></a>
                                        <div class="giasp">Giá tham khảo: <b>Liên hệ</b></div>

                                        <div class="cb"><!----></div>
                                    </div>
                                <?php $i++;?>
                                <?php endwhile;endif;wp_reset_query();?>

                            </div>
                            <div class="cb"><!----></div>
                        </div>
                        <div class="cb"><!----></div>
                    </div>
                    <div class="cb"><!----></div>

                </div>
            </div>


            <div class="cb"></div>

            <div id="SubServiceHome">
                <div class="BlockHeader">
                    <div class="Header">
                        <span class="Title"><a href="tin-tuc-va-su-kien-e.html" title="Tin tức và sự kiện" class="TextTitle">Tin tức và sự kiện</a></span>
                        <span class="Background"></span>
                        <span class="SeeMore"><a href="tin-tuc-va-su-kien-e.html" class="LinkSeeMore" title="Xem tất cả">Xem tất cả</a></span>
                    </div>
                    <div class="BlockItems">

                        <?php
                        $i = 0;
                        query_posts('cat=4&posts_per_page=8&orderby=id');
                        if (have_posts()):while (have_posts()):the_post();
                            ?>
                        <div class="Items Item_<?php if($i % 2 == 0) $i = 0; echo $i;?>">
                            <div class="khungAnhCropBg">
                                <a href="<?php the_permalink()?>" class="khungAnhCrop">
                                    <img alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url();?>" style="position: absolute; z-index: 1; top: 0px; left: -21px; height: 96px; width: 172px;">
                                </a>
                            </div>
                            <div class="ContentItem">
                                <h2 style="padding: 0;margin: 0;height: 34px;overflow: hidden;"><a style="font-size: 14px; color: black;" href="<?php the_permalink()?>" title="<?php the_title()?>" class="Text"><?php the_title()?></a></h2>
                                <div class="BorderBottomTitle"></div>
                                <div style="text-align: justify; line-height: 16px; font-size: 11px; color: gray;" class="InfoItem">
                                    <?php the_excerpt()?>
                                </div>
                            </div>
                            <div class="cb"></div>
                        </div>
                            <?php $i++;?>
                        <?php endwhile;endif;wp_reset_query();?>

                    </div>
                </div>
                <div class="cb"></div>
            </div>

        </div>
        <div class="cb h20"></div>
    </div>
</div>
<?php get_footer(); ?>