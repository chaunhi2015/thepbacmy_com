﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
	 <div class="clearfix" id="container">
    <div class="a-column">
		<h4 class="widgettitle"><?php
		if($_GET['lang']=='en'){
			echo "Product Catalog";
		}else{
			echo "Danh mục sản phẩm";
		}
	  ?></h4>
		<?php genesis_after_content(); ?> 
      
      <div class="clear"></div>
      <div class="topLeft"> &nbsp;</div>
      <div class="midleLeft"><span> 
	  <?php
		if($_GET['lang']=='en'){
			echo "Partners";
		}else{
			echo "Đối tác";
		}
	  ?>
	  </span></div>
      <div class="containLeft clearfix">
        <div class="tigia">
         <marquee onmouseover="this.stop()" onmouseout="this.start()" direction="up">
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/agilent.jpg" title="bottom 1" alt="bottom 1" width="188" height="190" /> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/buchi logo.jpg" title="bottom 1" alt="bottom 1"  width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/fisher logo.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo berghof.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo hach.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo horiba.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/> 
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo malvern.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo Merck.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo shimadzu.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/logo thermo.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
			  <img src="<?php bloginfo('stylesheet_directory')?>/images/panalytical.jpg" title="bottom 1" alt="bottom 1" width="188" height="70"/>
		  </marquee>
		</div>
	  </div>
      <div class="clear"></div>
	  
      <div class="topLeft"> &nbsp;</div>
      <div class="midleLeft"><span> <?php
		if($_GET['lang']=='en'){
			echo "Visitor Statistics";
		}else{
			echo "Thống kê truy cập";
		}
	  ?></span></div>
      <div class="containLeft clearfix">
        <p class="truycap">
			<!-- Histats.com  START (html only)-->
<a href="http://www.histats.com" alt="page hit counter" target="_blank" >
<embed src="http://s10.histats.com/406.swf"  flashvars="jver=1&acsid=2426904&domi=4"  quality="high"  width="165" height="100" name="406.swf"  align="middle" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" /></a>
<img  src="http://sstatic1.histats.com/0.gif?2426904&101" alt="web statistics" border="0">
<!-- Histats.com  END  -->
		</p>
      </div>
      
	  
    </div>
        <div class="contain"> <!-- InstanceBeginEditable name="Container" -->
      <div class="topBo"> &nbsp;</div>
      <div class="midBo"><span><?php single_cat_title();?></span></div>
      <div class="containCenter listTinTuc clearfix">
		<ul class="sanpham clearfix">
			<?php
				$i=0;
				
				if(have_posts()):while(have_posts()):the_post();				
			?>
          <li> 
			<a href="<?php the_permalink();?>" onmouseover="TagToTip('divProduct<?php echo $i;?>')" onmouseout="UnTip()"><?php if(function_exists("has_post_thumbnail") & has_post_thumbnail()){	?> 
					<?php the_post_thumbnail(array(180,142));?>
					
					<?php }?>
			</a>
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<div id="divProduct<?php echo $i;?>" style="display: none;">
				<?php the_post_thumbnail(array(300,300));?>
			</div>
          </li>
		  <?php
			$i++;
		  endwhile;endif;wp_reset_query();?>
          
        </ul>
      </div>
      <div class="paging">
		<?php wp_pagenavi(); ?>
	  
	  </div>
      <!-- InstanceEndEditable --></div>

	<div class="c-column">
		
      <?php genesis_after_content_sidebar_wrap(); ?>
      
	  <div class="clear"></div>
      <div class="topRight"> &nbsp;</div>
      <div class="midleRight"><span>
	  <?php
		if($_GET['lang']=='en'){
			echo "Products Technology Vietnam";
		}else{
			echo "Sản phẩm công nghệ Việt Nam";
		}
	  ?></span></div>
      <div class="pro_hot">
		<ul class="product_hot">
			<?php
				$i=100;
				if($_GET['lang']=='en'){
					query_posts('cat=45&posts_per_page=5&orderby=id');
				}else{
					query_posts('cat=9&posts_per_page=5&orderby=id');
				}
				if(have_posts()):while(have_posts()):the_post();				
			?>
          <li> 
			<a href="<?php the_permalink();?>" onmouseover="TagToTip('divProduct<?php echo $i;?>')" onmouseout="UnTip()"><?php if(function_exists("has_post_thumbnail") & has_post_thumbnail()){	?> 
					<?php the_post_thumbnail(array(180,142));?>
					
					<?php }?>
			</a>
			<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
			<div id="divProduct<?php echo $i;?>" style="display: none;">
				<?php the_post_thumbnail(array(200,200));?>
			</div>
          </li>
		  <?php
			$i++;
		  endwhile;endif;wp_reset_query();?>
		</ul>
	  
	  </div>
	  
      
    </div>
    <div class="clear"></div>
	
	<?php get_footer(); ?>