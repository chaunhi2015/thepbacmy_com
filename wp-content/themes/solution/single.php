<?php get_header(); ?>
    <div id="HomepageLoadControl">
        <div class="cb h10"></div>
        <div class="PageContentDetailLeft">

            <div id="SubProductMenu">
                <?php get_sidebar('primary');?>
            </div>
        </div>
        <div id="ProductDetail">

            <div class="LeftProductDetail">
                <div style="margin-left:10px;"></div>
                <div class="cb"></div>
                <div class="LeftProductDetail2">
                    <?php
                    if (have_posts()):while (have_posts()):the_post();
                        ?>

                    <div id="TopProductDetail">
                        <div class="h0"></div>
                        <div class="left">
                            <div id="DisplayLoadControl1_ctl00_ctl00_pnContent" class="pannel">
                                <img class="defaultImg lightbox" alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url(); ?>" ></a>
                                <div class="cb h10"></div>
                            </div>
                        </div>

                        <div class="Thongtinsanpham">
                            <h1><?php the_title()?></h1>
                            <div class="chitiet baogiamoi">Giá : <span class="giamoi">Liên hệ</span> (Đã có VAT)</div>
                            <div class="cb"></div>
                            <div class="chitiet baoconhang"><div class="otich"><span class="conhang"></span></div><span>Còn hàng</span></div>
                            <div class="cb"></div><div class="cb"></div>
                            <div class="cb h10"></div>

                        </div>
                    </div>
                    <div class="cb h20"></div>


                    <div class="tabbed_area">
                        <ul class="tabs">
                            <li><span title="content_1" class="tab active">MÔ TẢ SẢN PHẨM</span></li>
                            <!--<li><span title="content_2" class="tab">CHI TIẾT KỸ THUẬT</span></li>
                            <li style="display: none;"><span title="content_3" class="tab">BÌNH LUẬN SẢN PHẨM</span></li>-->
                        </ul>
                        <div id="content_1" class="noidung">
                            <?php the_content()?>
                        </div>
                        <!--<div id="content_2" class="noidung">
                            noi dung2
                        </div>
                        <div id="content_3" class="noidung">
                            noi dung 3
                        </div>-->
                        <div class="cb"></div>
                    </div>
                    <?php endwhile;endif;wp_reset_query();?>
                </div>
            </div>
            <div class="cb"></div>
            <div id="SubProductOtherItem">
                <div class="Nhomsanpham">
                    <div class="baosanphamindex">
                        <div class="cate">
                            <a href="#">Sản phẩm Liên quan</a><div class="vach"></div>
                        </div>
                        <div class="cb"></div>
                        <div class="listNews">
                            <div class="tronglist">
                                <div id="mycarouselImg2">
                                    <ul>
                                        <?php
                                            query_posts('cat=3&pid=-'.$post->ID.'&posts_per_page=8&orderby=id');
                                            if (have_posts()):while (have_posts()):the_post();
                                        ?>
                                        <li class="" style="overflow: hidden; float: left; width: 167px; height: 215px;">
                                            <div class="baosanpham thutu0">
                                                <div class="khungAnh">
                                                    <a class="khungAnhCrop" href="<?php the_permalink()?>">
                                                        <img alt="<?php the_title()?>" class="" src="<?php echo get_the_post_thumbnail_url(); ?>" style="position: absolute; z-index: 1; top: -5px; left: 0px; width: 162px; height: 130px;"></a>
                                                </div>
                                                <a class="title" href="<?php the_permalink()?>" title="<?php the_title()?>"><?php the_title()?></a>
                                                <div class="giasp">Giá tham khảo: <b>Liên hệ</b></div>

                                                <div class="cb"><!----></div>
                                            </div>
                                        </li>
                                            <?php endwhile;endif;wp_reset_query();?>
                                    </ul>
                                </div>
                                <div class="cb"><!----></div>
                            </div>
                            <div class="cb"><!----></div>
                        </div>
                        <div class="cb"><!----></div>
                    </div>
                    <div class="cb"><!----></div>
                </div>
            </div>
        </div>
        <div class="cb h15"></div>
    </div>

<?php get_footer(); ?>